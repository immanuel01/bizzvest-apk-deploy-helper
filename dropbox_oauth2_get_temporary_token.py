import dropbox
from dropbox.exceptions import AuthError

# Enter your app's client ID and refresh token
client_id = 'YOUR_CLIENT_ID'
refresh_token = 'YOUR_REFRESH_TOKEN'
def get_temporary_token(client_id, client_secret, refresh_token):
    # Create a Dropbox OAuth2 flow object with your app's client ID and secret
    flow = dropbox.DropboxOAuth2FlowNoRedirect(client_id, client_secret=client_secret)

    try:
        # Use the refresh token to get a new access token
        access_token, _ = flow.refresh_access_token(refresh_token)

        # Print the access token
        print("Temporary access token:", access_token)

    except AuthError as e:
        print('Error getting temporary access token:', e)